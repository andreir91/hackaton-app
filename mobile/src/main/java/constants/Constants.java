package constants;

/**
 * Created by Andrei on 06/06/15.
 */
public class Constants {
    public static final String STATUS_CHANGED = "status";
    public static final String WEAR_COMMAND = "wear_command";
}
