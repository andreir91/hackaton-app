package services;

import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Binder;
import android.os.Bundle;
import android.os.IBinder;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.wearable.MessageApi;
import com.google.android.gms.wearable.MessageEvent;
import com.google.android.gms.wearable.Node;
import com.google.android.gms.wearable.NodeApi;
import com.google.android.gms.wearable.Wearable;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.List;
import java.util.concurrent.TimeUnit;

import constants.Constants;

/**
 * Created by Andrei on 06/06/15.
 * <p/>
 * The service is responsible to manage the communication between the app and the server
 */
public class ConnectionService extends Service {
    private Socket socket;
    private BufferedReader serverReader;
    private PrintWriter serverWriter;
    private final IBinder connectionServiceBinder = new LocalBinder();

    public class LocalBinder extends Binder {
        public ConnectionService getService() {
            return ConnectionService.this;
        }
    }

    @Override
    public void onCreate() {
        LocalBroadcastManager.getInstance(this).registerReceiver(new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                String command = intent.getStringExtra("command");

                switch (command) {
                    case "lock":
                        lockCar();
                        break;
                    case "unlock":
                        unlockCar();
                        break;
                    case "location":
                        getLocation();
                    default:
                        break;
                }
            }
        }, new IntentFilter(Constants.WEAR_COMMAND));
    }

    @Override
    public IBinder onBind(Intent intent) {
        return connectionServiceBinder;
    }

    public void sendMessage(final GoogleApiClient client, final String message) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                client.blockingConnect(100, TimeUnit.MILLISECONDS);
                NodeApi.GetConnectedNodesResult result =
                        Wearable.NodeApi.getConnectedNodes(client).await();
                List<Node> nodes = result.getNodes();
                if (nodes.size() > 0) {
                    for (Node node : nodes) {
                        Wearable.MessageApi.sendMessage(client, node.getId(), "status", message.getBytes());
                    }
                }
                client.disconnect();
            }
        }).start();
    }

    /**
     * Connects to the host provided by the host name and port
     */
    public boolean initConnection() {
        // Create runnable object to run on a sepparate thread
        final Runnable runnable = new Runnable() {
            @Override
            public void run() {
                try {
                    String hostName = "10.0.0.178";
                    int port = 15187;

                    socket = new Socket(hostName, port);

                    serverReader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
                    serverWriter = new PrintWriter(socket.getOutputStream(), true);
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        };

        // Establish the connection on a sepparate thread
        Thread thread = new Thread(runnable);
        thread.start();
        return true;
    }

    /**
     * Gets the status of the car
     */
    public void getStatus() {
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                try {
                    String status = "";
                    if (socket == null) {
                        initConnection();
                    }
                    if (!socket.isConnected()) {
                        initConnection();
                    }

                    serverWriter.write("status\r\n");
                    serverWriter.flush();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        };

        Thread t = new Thread(runnable);
        t.start();
    }

    /**
     * Sends a "lock" command to the server
     */
    public void lockCar() {
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                if (socket == null) {
                    initConnection();
                }
                if (!socket.isConnected()) {
                    initConnection();
                }

                serverWriter.write("lock\r\n");
                serverWriter.flush();
            }
        };

        Thread t = new Thread(runnable);
        t.start();
    }

    /**
     * Sends an "unlock" command to the server
     */
    public void unlockCar() {
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                if (socket == null) {
                    initConnection();
                }
                if (!socket.isConnected()) {
                    initConnection();
                }
                serverWriter.write("unlock\r\n");
                serverWriter.flush();
            }
        };

        Thread t = new Thread(runnable);
        t.start();
    }

    public void getLocation() {
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                try {
                    if (socket == null) {
                        initConnection();
                    }
                    if (!socket.isConnected()) {
                        initConnection();
                    }
                    serverWriter.write("location\r\n");
                    serverWriter.flush();

                    String location = serverReader.readLine();

                    Intent intent = new Intent(Constants.STATUS_CHANGED);
                    intent.putExtra("status", location);
                    getApplicationContext().sendBroadcast(intent);

                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        };

        Thread t = new Thread(runnable);
        t.start();
    }

    public void readStatus() {
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                try {
                    do {
                        if (serverReader != null) {// && serverWriter != null) {
                            //serverWriter.write("status\r\n");
                            //serverWriter.flush();

                            String status = serverReader.readLine();
                            if (status == null) {
                                Thread.sleep(1000);
                                Log.d("STATUS", "Status from server is null");
                            } else if (status.length() > 10) {
                                Intent intent = new Intent(Constants.STATUS_CHANGED);
                                intent.putExtra("status", status);
                                getApplicationContext().sendBroadcast(intent);
                            }

                            // Thread.sleep(1000);
                        }
                    } while (true);
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        };

        Thread t = new Thread(runnable);
        t.start();
    }
}
