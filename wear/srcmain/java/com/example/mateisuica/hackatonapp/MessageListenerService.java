package com.example.mateisuica.hackatonapp;

import android.content.Intent;
import android.net.Uri;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.google.android.gms.wearable.MessageEvent;
import com.google.android.gms.wearable.WearableListenerService;

import java.net.URI;

/**
 * Created by mateisuica on 06/06/15.
 */
public class MessageListenerService extends WearableListenerService {

    @Override
    public void onMessageReceived(MessageEvent messageEvent) {

        CarDataSingleton carDataSingleton = CarDataSingleton.getInstance();
        String data = new String(messageEvent.getData());

        switch (messageEvent.getPath()) {
            case "status":
                String dataArray[] = data.split("#");
                // Check lock state
                carDataSingleton.setLock(dataArray[1].contains("1"));

                // Check alarm state
                carDataSingleton.setAlarm(dataArray[2].contains("1"));

                // Check door state
                carDataSingleton.setDoors(dataArray[3].contains("1"));

                // Set temperature text view
                carDataSingleton.setTemperature(Integer.parseInt(dataArray[4].substring(5)));
                Intent intent = new Intent("refresh-status");
                LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
                break;
            case "location":
                Intent mapIntent = new Intent();
                mapIntent.setAction(Intent.ACTION_VIEW);
                String locationData = data.substring(data.indexOf("="));
                mapIntent.setData(Uri.parse("geo:" + locationData));
                break;

        }

        super.onMessageReceived(messageEvent);
    }

}