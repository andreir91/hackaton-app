//
//  InterfaceController.h
//  SmartCar WatchKit Extension
//
//  Created by Boca Andrei on 06/06/2015.
//  Copyright (c) 2015 Boca Andrei. All rights reserved.
//

#import <WatchKit/WatchKit.h>
#import <Foundation/Foundation.h>
#import <myAppFramework/SCSocket.h>

@interface InterfaceController : WKInterfaceController <SCSocketDelegate>

+(SCSocket *)getSocket;

@end
