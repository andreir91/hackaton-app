package com.example.mateisuica.hackatonapp;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.wearable.activity.ConfirmationActivity;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.wearable.Node;
import com.google.android.gms.wearable.NodeApi;
import com.google.android.gms.wearable.Wearable;

import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * Created by mateisuica on 06/06/15.
 */
public class LockControlActiviry extends Activity {

    private GoogleApiClient client;
    private long CONNECTION_TIME_OUT_MS = 100;
    private String command = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.command_activity);

        initApi();

        TextView headerText = (TextView) findViewById(R.id.headerText);
        Intent intent = getIntent();
        if(intent != null) {
            final String title = intent.getStringExtra(MainActivity.COMMAND);
            CarDataSingleton carDataSingleton = CarDataSingleton.getInstance();
            switch (title) {

                case MainActivity.LOCK:
                    command = carDataSingleton.isLock() ? "unlock" : "lock";
                    headerText.setText("Car is " + (carDataSingleton.isLock() ? "locked." : "unlocked.") + "Do you want to " +
                            (carDataSingleton.isLock() ? "unlock?" : "lock?")
                    );
                    break;
                case MainActivity.FIND_MY_CAR:
                    command = "location";
                    headerText.setText("Do you want to find you car?");
                    break;
            }




            ImageButton ok = (ImageButton) findViewById(R.id.ok_button);
            ok.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    sendToast(command);

                    Intent intent = new Intent(LockControlActiviry.this, ConfirmationActivity.class);
                    intent.putExtra(ConfirmationActivity.EXTRA_ANIMATION_TYPE,
                            ConfirmationActivity.SUCCESS_ANIMATION);
                    intent.putExtra(ConfirmationActivity.EXTRA_MESSAGE,
                            getString(R.string.msg_sent));
                    startActivity(intent);
                    finish();
                }
            });
            ImageButton cancel = (ImageButton) findViewById(R.id.cancel_button);
            cancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    finish();
                }
            });
        }

    }


    private GoogleApiClient getGoogleApiClient(Context context) {
        return new GoogleApiClient.Builder(context)
                .addApi(Wearable.API)
                .build();
    }


    /**
     * Initializes the GoogleApiClient and gets the Node ID of the connected device.
     */
    private void initApi() {
        client = getGoogleApiClient(this);
    }

    /**
     * Sends a message to the connected mobile device, telling it to show a Toast.
     */
    private void sendToast(final String text) {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    client.blockingConnect(CONNECTION_TIME_OUT_MS, TimeUnit.MILLISECONDS);
                    NodeApi.GetConnectedNodesResult result =
                            Wearable.NodeApi.getConnectedNodes(client).await();
                    List<Node> nodes = result.getNodes();
                    for(Node node : nodes) {
                        Wearable.MessageApi.sendMessage(client, node.getId(), text, null);
                    }
                    client.disconnect();
                }
            }).start();

    }
}
