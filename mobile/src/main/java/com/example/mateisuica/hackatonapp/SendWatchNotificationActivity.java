package com.example.mateisuica.hackatonapp;

import android.app.Activity;
import android.app.PendingIntent;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;
import android.view.View;
import android.widget.Button;

/**
 * Created by mateisuica on 06/06/15.
 */
public class SendWatchNotificationActivity extends Activity {

    public static String EXTRA_EVENT_ID = "extraEventId";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.notification_sender);
        Button button = (Button) findViewById(R.id.sendNotificationButton);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int notificationId = 001;
                // Build intent for notification content
                Intent viewIntent = new Intent(SendWatchNotificationActivity.this, ReceiveWatchNotificationActivity.class);
                viewIntent.putExtra(EXTRA_EVENT_ID, Math.random()*100);
                PendingIntent viewPendingIntent =
                        PendingIntent.getActivity(SendWatchNotificationActivity.this, 0, viewIntent, 0);
                long[] pattern = {0, 100, 1000};
                NotificationCompat.Builder notificationBuilder =
                        new NotificationCompat.Builder(SendWatchNotificationActivity.this)
                                .setSmallIcon(R.drawable.ic_plusone_standard_off_client)
                                .setContentTitle("You got a notification")
                                .setContentText("Succesfully sent notification!")
                                .setVibrate(pattern)
                                .setContentIntent(viewPendingIntent);

// Get an instance of the NotificationManager service
                NotificationManagerCompat notificationManager =
                        NotificationManagerCompat.from(SendWatchNotificationActivity.this);

// Build the notification and issues it with notification manager.
                notificationManager.notify(notificationId, notificationBuilder.build());
            }
        });
    }
}
