//
//  DoorOpenInterfaceController.m
//  myApp
//
//  Created by Boca Andrei on 07/06/2015.
//  Copyright (c) 2015 Boca Andrei. All rights reserved.
//

#import "DoorOpenInterfaceController.h"

@interface DoorOpenInterfaceController ()

@end

@implementation DoorOpenInterfaceController

- (IBAction)yesAction {
    [self popToRootController];
}

- (IBAction)noAction {
    [self popToRootController];
}

- (void)awakeWithContext:(id)context {
    [super awakeWithContext:context];
    
    // Configure interface objects here.
}

- (void)willActivate {
    // This method is called when watch view controller is about to be visible to user
    [super willActivate];
}

- (void)didDeactivate {
    // This method is called when watch view controller is no longer visible
    [super didDeactivate];
}

@end



