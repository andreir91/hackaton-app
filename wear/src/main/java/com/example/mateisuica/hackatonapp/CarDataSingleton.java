package com.example.mateisuica.hackatonapp;

/**
 * Created by mateisuica on 06/06/15.
 */
public class CarDataSingleton {

    private static CarDataSingleton instance;

    private boolean alarm;
    private boolean lock;
    private boolean doors;
    private int temperature;
    private float latitude;
    private float longitude;

    private CarDataSingleton() {
    }

    public static CarDataSingleton getInstance() {
        if(instance == null) {
            instance = new CarDataSingleton();
        }

        return instance;
    }


    public boolean isAlarm() {
        return alarm;
    }

    public boolean isLock() {
        return lock;
    }

    public boolean isDoors() {
        return doors;
    }

    public int getTemperature() {
        return temperature;
    }

    public static void setInstance(CarDataSingleton instance) {
        CarDataSingleton.instance = instance;
    }

    public void setAlarm(boolean alarm) {
        this.alarm = alarm;
    }

    public void setLock(boolean lock) {
        this.lock = lock;
    }

    public void setDoors(boolean doors) {
        this.doors = doors;
    }

    public void setTemperature(int temperature) {
        this.temperature = temperature;
    }

    public float getLatitude() {
        return latitude;
    }

    public void setLatitude(float latitude) {
        this.latitude = latitude;
    }

    public float getLongitude() {
        return longitude;
    }

    public void setLongitude(float longitude) {
        this.longitude = longitude;
    }
}
