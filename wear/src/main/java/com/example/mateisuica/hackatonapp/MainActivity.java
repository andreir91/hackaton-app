package com.example.mateisuica.hackatonapp;

import android.app.Activity;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.os.Bundle;
import android.support.wearable.view.CardFragment;
import android.support.wearable.view.WatchViewStub;
import android.support.wearable.view.WearableListView;
import android.widget.ImageView;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends Activity implements WearableListView.ClickListener {

    // Sample dataset for the list
    List<ListItemDataHolder> elements = new ArrayList<>();

    public static final String COMMAND = "Command";
    public static final String STATUS = "Status";
    public static final String LOCK = "Lock";
    public static final String FIND_MY_CAR = "Find my car";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        // Get the list component from the layout of the activity
        WearableListView listView =
                (WearableListView) findViewById(R.id.wearable_list);

        elements.add(new ListItemDataHolder(STATUS, R.drawable.ic_status));
        elements.add(new ListItemDataHolder(LOCK, R.drawable.ic_lock));
        elements.add(new ListItemDataHolder(FIND_MY_CAR, R.drawable.ic_find_my_car));
        // Assign an adapter to the list
        listView.setAdapter(new MyListAdapter(this, elements));

        // Set a click listener
        listView.setClickListener(this);
    }

    // WearableListView click listener
    @Override
    public void onClick(WearableListView.ViewHolder v) {
        Integer tag = (Integer) v.itemView.getTag();
        // use this data to complete some action ...

        String command = elements.get(tag).title;

        Intent intent = null;
        switch (command) {
            case LOCK:
            case FIND_MY_CAR:
                intent = new Intent(this, CommandActivity.class);
                intent.putExtra(COMMAND, command);
                break;
            case STATUS:
                intent = new Intent(this, StatusActivity.class);
                break;
        }
        startActivity(intent);

    }

    @Override
    public void onTopEmptyRegionClick() {
    }
}