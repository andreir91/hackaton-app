//
//  MapInterfaceController.m
//  myApp
//
//  Created by Boca Andrei on 07/06/2015.
//  Copyright (c) 2015 Boca Andrei. All rights reserved.
//

#import "MapInterfaceController.h"
#import "InterfaceController.h"
@interface MapInterfaceController ()
@property (weak, nonatomic) IBOutlet WKInterfaceMap *map;
@property (strong, nonatomic) SCSocket *comSocket;
@end

@implementation MapInterfaceController

- (void)awakeWithContext:(id)context {
    [super awakeWithContext:context];
    
    // Configure interface objects here.
}

- (void)willActivate {
    
   self.comSocket = [InterfaceController getSocket];
    if (self.comSocket) {
        [self.comSocket addDelegate:self];
        [self.comSocket sendCommand:SCSocketCommandsLocation];
    }
    // This method is called when watch view controller is about to be visible to user
    [super willActivate];
}

- (void)didDeactivate {
    [self.comSocket removeDelegate:self];
    self.comSocket = nil;
    // This method is called when watch view controller is no longer visible
    [super didDeactivate];
}

- (void)socket:(SCSocket *)socket receivedData:(NSString *)dataReceived {
    if ([dataReceived containsString:@"location"]) {
        NSRange rangeofLoc = [dataReceived rangeOfString:@"location="];
        NSArray *coordonate = [[dataReceived substringFromIndex:(rangeofLoc.location + rangeofLoc.length)] componentsSeparatedByString:@","];
        
        CLLocationCoordinate2D coordonates = CLLocationCoordinate2DMake([coordonate[0] doubleValue],[coordonate[1] doubleValue]);
        [self.map setRegion:MKCoordinateRegionMakeWithDistance(coordonates, 500 , 500)];
        [self.map addAnnotation:coordonates withPinColor:WKInterfaceMapPinColorRed];
    }
}

@end



