//
//  InterfaceController.m
//  SmartCar WatchKit Extension
//
//  Created by Boca Andrei on 06/06/2015.
//  Copyright (c) 2015 Boca Andrei. All rights reserved.
//

#import "InterfaceController.h"

typedef NS_ENUM(NSUInteger, kSMLockState) {
    kSMLockStateLock,
    kSMLockStateUnlock,
};

static NSString *statusLockString = @"Lock";
static NSString *statusUnLockString = @"Unlock";

@interface InterfaceController()
@property (weak, nonatomic) IBOutlet WKInterfaceLabel *alarmLabel;
@property (weak, nonatomic) IBOutlet WKInterfaceLabel *tempLabel;
@property (weak, nonatomic) IBOutlet WKInterfaceImage *carStateOrb;
@property (weak, nonatomic) IBOutlet WKInterfaceButton *lockingButton;
@property (assign, nonatomic) kSMLockState lockState;

@property (nonatomic, strong)SCSocket *communicationSocket;

@end

@implementation InterfaceController

static SCSocket *_socket;

+(SCSocket *)getSocket {
    return _socket;
}

- (void)awakeWithContext:(id)context {
    [super awakeWithContext:context];
    
    [self.carStateOrb setHidden:YES];
    self.communicationSocket = [SCSocket new];
    [self.communicationSocket startSocket];
    //[self.communicationSocket setContinuosString:@"status\r\n"];
    [self.communicationSocket addDelegate:self];
    _socket = self.communicationSocket;
    // Configure interface objects here.
}

- (void)willActivate {
    // This method is called when watch view controller is about to be visible to user
    [super willActivate];
}

- (void)didDeactivate {
    // This method is called when watch view controller is no longer visible
    [super didDeactivate];
}
- (IBAction)lockUnlock {
    if (kSMLockStateLock == self.lockState ) {
        [self.communicationSocket sendCommand:SCSocketCommandsUnlock];
    } else {
        [self.communicationSocket sendCommand:SCSocketCommandsLock];
    }
}

- (void)setLockState:(kSMLockState)lockState {
    _lockState = lockState;
    switch (lockState) {
        case kSMLockStateLock:
            [self.lockingButton setTitle:statusUnLockString];
            break;
        case kSMLockStateUnlock:
        default:
            [self.lockingButton setTitle:statusLockString];
            break;
    }
}

- (void)socket:(SCSocket *)socket receivedData:(NSString *)dataReceived {
    
    if ([dataReceived containsString:@"status"]) {
        [self.carStateOrb setHidden:NO];
        NSArray *data = [dataReceived componentsSeparatedByString:@"#"];
        //lockState
        if ([data[1] containsString:@"0"]) {
            [self setLockState:kSMLockStateUnlock];
        } else {
            [self setLockState:kSMLockStateLock];
        }
        //doorState
        NSString *resourceCar = ([data[3] containsString:@"0"] ? @"opened" : @"closed");
        
        //alarmState
        NSString *alarmStr = ([data[2] containsString:@"1"] ? @"" : @"OK");
        [self.alarmLabel setText:([data[2] containsString:@"1"] ? @"Alarm!" : @"")];
        [self.alarmLabel setHidden:![data[2] containsString:@"1"]];
        [self.tempLabel setHidden:[data[2] containsString:@"1"]];
        
        [self.carStateOrb setImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@%@",resourceCar,alarmStr]]];
        
        NSMutableAttributedString *tempString = [[NSMutableAttributedString alloc] init];
        [tempString appendAttributedString:[[NSAttributedString alloc] initWithString:@"°C" attributes:@{NSForegroundColorAttributeName:[UIColor clearColor]}]];
        
        NSString *string = [NSString stringWithFormat:@"%.0f", [[data[4] componentsSeparatedByString:@"="][1] doubleValue]];
        
        [tempString appendAttributedString:[[NSAttributedString alloc] initWithString:string attributes:@{NSForegroundColorAttributeName:[UIColor whiteColor]}]];
        [tempString appendAttributedString:[[NSAttributedString alloc] initWithString:@"°C" attributes:@{NSForegroundColorAttributeName:[UIColor whiteColor]}]];
        [self.tempLabel setAttributedText:tempString];
        
        
    } else if ([dataReceived containsString:@"unlock"]) {
        if ([dataReceived containsString:@"OK"]) {
            [self setLockState:kSMLockStateUnlock];
        }
    } else if ([dataReceived containsString:@"lock"]) {
        if ([dataReceived containsString:@"OK"]) {
           [self setLockState:kSMLockStateLock];
        }
    }
}

@end



