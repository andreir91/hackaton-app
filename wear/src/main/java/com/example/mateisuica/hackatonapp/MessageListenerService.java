package com.example.mateisuica.hackatonapp;

import android.content.Intent;
import android.location.Location;
import android.net.Uri;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.google.android.gms.wearable.MessageEvent;
import com.google.android.gms.wearable.WearableListenerService;

import java.util.Locale;

/**
 * Created by mateisuica on 06/06/15.
 */
public class MessageListenerService extends WearableListenerService {

    @Override
    public void onMessageReceived(MessageEvent messageEvent) {

        CarDataSingleton carDataSingleton = CarDataSingleton.getInstance();
        String status = new String(messageEvent.getData());
        switch (messageEvent.getPath()) {
            case "status":
                    if(!status.isEmpty()) {
                        String statusArray[] = status.split("#");

                        // Check lock state
                        carDataSingleton.setLock(statusArray[1].contains("1"));

                        // Check alarm state
                        carDataSingleton.setAlarm(statusArray[2].contains("1"));

                        // Check door state
                        carDataSingleton.setDoors(statusArray[3].contains("1"));

                        // Set temperature text view
                        carDataSingleton.setTemperature(Integer.parseInt(statusArray[4].substring(5).trim()));
                        Intent intent = new Intent("refresh-status");
                        LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
                    }
                break;


        }

        super.onMessageReceived(messageEvent);
    }

}