package com.example.mateisuica.hackatonapp;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.location.Location;
import android.media.Image;
import android.net.Uri;
import android.os.Bundle;
import android.os.IBinder;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Switch;
import android.widget.TextView;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.wearable.Wearable;

import java.util.Locale;

import constants.Constants;
import services.ConnectionService;

public class MainActivity extends Activity {

    private final BroadcastReceiver serverStatusChangedBroadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String status = intent.getExtras().get("status").toString();

            if (!status.isEmpty() && !status.contains("location")) {
                updateVisualElements(status);
            } else if (status.contains("location")) {
                String[] locationArray = status.split("=");
                location = new Location("");
                location.setLatitude(Double.parseDouble(locationArray[1].substring(0, locationArray[1].indexOf(","))));
                location.setLongitude(Double.parseDouble(locationArray[1].substring(locationArray[1].indexOf(",") + 1)));

                showLocationMap();
            }
        }
    };

    // Visual members
    private ImageView mDoorStatusImageView;
    private TextView mTemperatureTextView;
    private Switch mLockCarSwitch;
    private ImageView mAlarmCarSwitch;
    private TextView mCarLockLabel;
    private Button mLocationButton;
    private int lockStatus;
    private Location location;

    // Service connection members
    private ConnectionService mService;
    private boolean mBound;

    private GoogleApiClient googleApiClient;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Register Broadcast Receiver
        final IntentFilter serverStatusChangedIntentFilter = new IntentFilter();
        serverStatusChangedIntentFilter.addAction(Constants.STATUS_CHANGED);
        registerReceiver(serverStatusChangedBroadcastReceiver, serverStatusChangedIntentFilter);

        mLockCarSwitch = (Switch) findViewById(R.id.car_lock_switch);
        mLockCarSwitch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (lockStatus == 0) {
                    mService.lockCar();
                } else {
                    mService.unlockCar();
                }
            }
        });

        mAlarmCarSwitch = (ImageView) findViewById(R.id.car_alarm_switch);
        mAlarmCarSwitch.setEnabled(false);

        mDoorStatusImageView = (ImageView) findViewById(R.id.car_door_image_view);
        mTemperatureTextView = (TextView) findViewById(R.id.temperature_text_view);

        mCarLockLabel = (TextView) findViewById(R.id.car_lock_label);

        mLocationButton = (Button) findViewById(R.id.show_location_button);
        mLocationButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mService.getLocation();
            }
        });

        // Connect google api client for messaging with wearable
        googleApiClient = new GoogleApiClient.Builder(this)
                .addApi(Wearable.API)
                .build();

        googleApiClient.connect();
    }

    private void showLocationMap(){
        if (location != null) {
            String uri = String.format(Locale.ENGLISH, "geo:%f,%f", location.getLatitude(), location.getLongitude());
            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(uri));
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            getApplicationContext().startActivity(intent);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onStart() {
        super.onStart();
        // Bind to LocalService
        Intent intent = new Intent(this, ConnectionService.class);
        bindService(intent, mConnection, Context.BIND_AUTO_CREATE);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        unregisterReceiver(serverStatusChangedBroadcastReceiver);
        unbindService(mConnection);
    }

    private ServiceConnection mConnection = new ServiceConnection() {

        @Override
        public void onServiceConnected(ComponentName className, IBinder service) {
            // We've bound to LocalService, cast the IBinder and get LocalService instance
            ConnectionService.LocalBinder binder = (ConnectionService.LocalBinder) service;
            mService = binder.getService();
            mBound = true;

            mService.initConnection();
            mService.readStatus();
        }

        @Override
        public void onServiceDisconnected(ComponentName arg0) {
            mBound = false;
        }
    };

    private void updateVisualElements(String status) {
        String statusArray[] = status.split("#");
        mService.sendMessage(googleApiClient, status);

        // Check lock state
        if (statusArray[1].contains("1")) {
            mLockCarSwitch.setChecked(true);
            mCarLockLabel.setText(R.string.toggle_status_unlock_label);
            lockStatus = 1;
        } else {
            mLockCarSwitch.setChecked(false);
            mCarLockLabel.setText(R.string.toggle_status_lock_label);
            lockStatus = 0;
        }

        // Check alarm state
        if (statusArray[2].contains("1")) {
            mAlarmCarSwitch.setImageDrawable(getResources().getDrawable(R.drawable.alarm));
        } else {
            mAlarmCarSwitch.setImageDrawable(null);
        }

        // Check door state
        if (statusArray[3].contains("1")) {
            mDoorStatusImageView.setImageDrawable(getResources().getDrawable(R.drawable.closed));
        } else {
            mDoorStatusImageView.setImageDrawable(getResources().getDrawable(R.drawable.opened));
        }

        // Set temperature text view
        mTemperatureTextView.setText(statusArray[4].substring(5));
    }
}
