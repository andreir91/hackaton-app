//
//  SCSocket.h
//  SmartCar
//
//  Created by Boca Andrei on 06/06/2015.
//  Copyright (c) 2015 Boca Andrei. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef NS_ENUM(NSUInteger, SCSocketCommands) {
    SCSocketCommandsGetState,
    SCSocketCommandsLock,
    SCSocketCommandsUnlock,
    SCSocketCommandsLocation
};

@protocol SCSocketDelegate;
@interface SCSocket : NSObject <NSStreamDelegate>

@property(nonatomic, weak)NSString *continuosString;

- (void)startSocket;
- (void)stopSocket;
- (void)sendCommand:(SCSocketCommands)command;

- (void)addDelegate:(id<SCSocketDelegate>)delegate;
- (void)removeDelegate:(id<SCSocketDelegate>)delegate;

@end

@protocol SCSocketDelegate <NSObject>

- (void)socket:(SCSocket *)socket receivedData:(NSString *)dataReceived;

@end
