﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Sockets;
using System.Threading;
using System.Net;
using System.IO;
using System.IO.Ports;

namespace Server {
    class TCPServer {
        private TcpListener tcpListener;
        private Thread listenThread;
        private Thread looperThread;
        private Thread statusSender;
        private SerialPort SP1;

        private bool shouldLock = false, shouldUnlock = false;        
        private string status = "none";
        private string location = "location=46.766829,23.586034", location2 = "location=46.7707625,23.5970906";

        public TCPServer() {
            this.tcpListener = new TcpListener(IPAddress.Any, 15187);
            this.listenThread = new Thread(new ThreadStart(ListenForClients));
            this.looperThread = new Thread(new ThreadStart(processCar));
        }

        public void Start() {
            Console.WriteLine("Starting server");
            Console.WriteLine("Done.");
            try {
                SP1 = new SerialPort("/dev/ttyAMA0", 115200, Parity.None, 8, StopBits.One);
                SP1.Open();
                SP1.ReadTimeout = 10000;
                Console.WriteLine("Serial port Opened");
                Thread.Sleep(500);
                SP1.Write("3");
                Thread.Sleep(1000);
                SP1.Write("2");
                //SP1 = new SerialPort("COM5", 115200, Parity.None, 8, StopBits.One);
                //SP1.Open();
                //SP1.ReadTimeout = 10000;
                //Console.WriteLine("Serial port Opened");
                //Thread.Sleep(1000);
                looperThread.Start();
            }
            catch (Exception) {
                Console.WriteLine("Could not Open Serial Port");
            }
            this.listenThread.Start();
        }

        private void ListenForClients() {
            this.tcpListener.Start();
            while (true) {
                //blocks until a client has connected to the server
                TcpClient client = this.tcpListener.AcceptTcpClient();

                //create a thread to handle communication with connected client             
                Thread clientThread = new Thread(new ParameterizedThreadStart(HandleClientComm));
                clientThread.Start(client);
            }
        }

        private string readDataFromSP() {
            try {
                if (SP1.IsOpen) {
                    return SP1.ReadLine();
                }
                else {
                    Console.WriteLine("Retuned 'null' from 'else'");
                    return "null";
                }
            }
            catch (Exception) {
                Console.WriteLine("Retuned 'null' from 'exception'");
                return "null";
            }
        }

        private string getStatus() {
            SP1.Write("1");
            string response = readDataFromSP();
            //parsing status
            string[] status = response.Split('#');
            string lock_state = "#lockState=";
            string alarm_state = "#alarmState=";
            string door_state = "#doorState=";
            string temp = "#temp=";
            if (status.Length > 3) {
                lock_state += status[1];
                alarm_state += status[2];
                door_state += status[3];
                temp += status[4];
            }
            string theStatus = lock_state + alarm_state + door_state + temp;
            return theStatus;
            //alarm state (1-alarm, 0-quiet),
            //door state (1-closed, 0-open),
            //temp - temperature.
        }

        private void sendStatus(object statusO) {
            StatusObject statusObj = (StatusObject)statusO;           
            try {
                statusObj.writer.WriteLine("status " + status);
            }
            catch (Exception) {
               //Nothing here;
            }
            string newStatus = "none";
            int counter = 0;
            while (true) {
                try {
                    if (newStatus != status) {                        
                        newStatus = status;
                        Console.WriteLine("New Status to " + statusObj.clientName + " -> " + status);
                        statusObj.writer.WriteLine("status " + status);                        
                        counter = 0;
                    }
                    else {
                        counter++;
                    }
                    if (counter > 30) {
                        counter = 0;
                        Console.WriteLine("New Status to " + statusObj.clientName + " -> " + status);
                        statusObj.writer.WriteLine("status " + status);
                    }
                    Thread.Sleep(100);
                }
                catch (Exception) {
                   //Nothing here
                }               
            }
        }

        private void processCar() {
            while (true) {
                try {
                    string response = "";
                    Thread.Sleep(1000);
                    if (shouldLock == true) {
                        Console.WriteLine("Should lock :  TRUE");
                        shouldLock = false;
                        SP1.Write("2");
                        response = readDataFromSP();
                    }
                    else if (shouldUnlock == true) {
                        Console.WriteLine("Should unlock :  TRUE");
                        shouldUnlock = false;
                        SP1.Write("3");
                        response = readDataFromSP();
                    }
                    //Get new status
                    Thread.Sleep(30);
                    status = getStatus();
                    //Console.WriteLine("Get Status");
                }
                catch (Exception) {
                    Console.WriteLine("SP error");
                    break;
                }
            }
        }

        private void HandleClientComm(object client) {
            Console.WriteLine("Client connected.");
            TcpClient tcpClient = (TcpClient)client;
            NetworkStream clientStream = tcpClient.GetStream();
            StreamReader reader = new StreamReader(clientStream);
            StreamWriter writer = new StreamWriter(clientStream);
            writer.AutoFlush = true;
            String inData;
            string clientName = "";
            int locationIndex = 0;
            statusSender = new Thread(new ParameterizedThreadStart(sendStatus));
            StatusObject statusObj = new StatusObject(writer, "client");
            statusSender.Start(statusObj);

            while (true) {
                inData = "";
                try {
                    //blocks until a client sends a message                   
                    inData = reader.ReadLine();                    
                }
                catch {
                    //a socket error has occured
                    break;
                }
                if (inData == null || inData == "null") {
                    break;
                }
                else if (inData.Length > 0) {
                    Console.WriteLine("In from Client -> " + inData);
                    if (inData.Contains("unlock")) {
                        shouldUnlock = true;
                        writer.WriteLine("unlock OK");
                    }
                    else if (inData.Contains("lock")) {
                        shouldLock = true;
                        writer.WriteLine("lock OK");
                    }
                    else if (inData.Contains("location")) {
                        if (locationIndex == 0) {
                            locationIndex = 1;
                            writer.WriteLine(location);
                        }
                        else {
                            locationIndex = 0;
                            writer.WriteLine(location2);
                        }                        
                    }
                }
            }

            Console.WriteLine(clientName + " Disconnected!");
            tcpClient.Close();
            statusSender.Abort();
        }

    }
}
