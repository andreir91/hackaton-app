//
//  SCSocket.m
//  SmartCar
//
//  Created by Boca Andrei on 06/06/2015.
//  Copyright (c) 2015 Boca Andrei. All rights reserved.
//


#import "SCSocket.h"

static NSString *kSCSocketHost = @"10.0.0.178";
static NSInteger kSCSocketPort = 15187;

static NSString *kSCSocketOKString = @"ios\r\n";


@interface SCSocket ()

@property(nonatomic, strong)NSInputStream *inputStreamSocket;
@property(nonatomic, strong)NSOutputStream *outputStreamSocket;
@property(nonatomic, assign)NSInteger count;
@property(atomic, strong)NSString *writeString;
@property(nonatomic, strong)NSMutableArray *delegateArray;


@property(nonatomic, strong)NSThread *zaThread;

@end

@implementation SCSocket {
    BOOL _stringReceived;
}

- (NSMutableArray *)delegateArray {
    if (!_delegateArray) {
        _delegateArray = [NSMutableArray new];
    }
    return _delegateArray;
}

- (void)addDelegate:(id<SCSocketDelegate>)delegate {
    [self.delegateArray addObject:delegate];
}

- (void)removeDelegate:(id<SCSocketDelegate>)delegate {
    [self.delegateArray removeObject:delegate];
}

- (void)runningLoopRead {
    while (YES) {
        if (_stringReceived) {
            NSUInteger length_of_buffer = 1000;
            uint8_t *buffer = malloc(length_of_buffer * sizeof(uint8_t));
            NSUInteger numberOfbytesRead = [self.inputStreamSocket read:buffer maxLength:length_of_buffer];
            if (numberOfbytesRead > 5) {
                NSString *stringReceived = [[NSString alloc] initWithBytes:buffer
                                                                    length:numberOfbytesRead
                                                                  encoding:NSUTF8StringEncoding];
                NSLog(@"mesage received %@",stringReceived);
                __weak __typeof(self) weakSelf = self;
                [self runOnMainThread:^{
                    __strong __typeof(self) strongSelf = weakSelf;
                    for (id<SCSocketDelegate> delegateOne in strongSelf.delegateArray) {
                        [delegateOne socket:strongSelf receivedData:stringReceived];
                    }
                }];
            }
            _stringReceived = NO;
        } else {
            [NSThread sleepForTimeInterval:0.2];
        }
    }
}

- (void)runningLoopWrite {
    while (YES) {
        [NSThread sleepForTimeInterval:1];
        NSString *stringToUse = (self.writeString ? self.writeString : (self.continuosString ? self.continuosString : nil));
        self.writeString = nil;
        
        if (self.outputStreamSocket.hasSpaceAvailable && stringToUse) {
            NSInteger lengthOfString = 0;
            const uint8_t *startString = [self convertString:stringToUse length:&lengthOfString];
            [self.outputStreamSocket write:startString maxLength:lengthOfString];
            NSLog(@"mesage send %@",stringToUse);
        }
    }
}


- (void)sendCommand:(SCSocketCommands)command {
    switch (command) {
        case SCSocketCommandsGetState:
            self.writeString = @"status\r\n";
            break;
        case SCSocketCommandsLock:
            self.writeString = @"lock\r\n";
            break;
        case SCSocketCommandsUnlock:
            self.writeString = @"unlock\r\n";
            break;
        case SCSocketCommandsLocation:
            self.writeString = @"location\r\n";
            break;
        default:
            self.writeString = nil;
            break;
    }
}

- (void)startSocket {
    
    CFReadStreamRef readStream;
    CFWriteStreamRef writeStream;
    CFStreamCreatePairWithSocketToHost(NULL, (__bridge CFStringRef)kSCSocketHost, (unsigned int)kSCSocketPort, &readStream, &writeStream);
    
    NSInputStream *inputStream = (__bridge_transfer NSInputStream *)readStream;
    NSOutputStream *outputStream = (__bridge_transfer NSOutputStream *)writeStream;
    [inputStream setDelegate:self];
    [outputStream setDelegate:self];
    [inputStream scheduleInRunLoop:[NSRunLoop currentRunLoop] forMode:NSDefaultRunLoopMode];
    [outputStream scheduleInRunLoop:[NSRunLoop currentRunLoop] forMode:NSDefaultRunLoopMode];
    [inputStream open];
    [outputStream open];
    
    /* Store a reference to the input and output streams so that
     they don't go away.... */
    
    self.inputStreamSocket = inputStream;
    self.outputStreamSocket = outputStream;
    self.count = 0;
    
    [NSThread detachNewThreadSelector:@selector(runningLoopWrite) toTarget:self withObject:nil];
    [NSThread detachNewThreadSelector:@selector(runningLoopRead) toTarget:self withObject:nil];
}

- (void)stopSocket {
    [self.inputStreamSocket close];
    [self.outputStreamSocket close];
}

#pragma mark - NSSocketDelegate
- (void)stream:(NSStream *)aStream handleEvent:(NSStreamEvent)eventCode {
    
    NSString *streamName = (aStream == _outputStreamSocket ? @"outputStrem" : @"inputStrem");
    NSString *event = nil;
    switch (eventCode) {
        case NSStreamEventErrorOccurred:
            event = @"NSStreamEventErrorOccurred";
            break;
        case NSStreamEventOpenCompleted:
            event = @"NSStreamEventOpenCompleted";
            if (aStream == _outputStreamSocket) {
                self.writeString = kSCSocketOKString;
            }
            break;
        case NSStreamEventEndEncountered:
            event = @"NSStreamEventEndEncountered";
            break;
        case NSStreamEventHasBytesAvailable:
            event = @"NSStreamEventHasBytesAvailable";
            _stringReceived = YES;
            break;
        case NSStreamEventHasSpaceAvailable:
            event = @"NSStreamEventHasSpaceAvailable";
            break;
        default:
            break;
    }
    NSLog(@"%@",[NSString stringWithFormat:@"SCSocket %@ event: %@",streamName,event]);
}


- (const uint8_t *)convertString:(NSString *)theString length:(NSInteger *)lengthPointer{
    NSData *someData = [theString dataUsingEncoding:NSUTF8StringEncoding];
    const void *bytes = [someData bytes];
    
    if (lengthPointer) {
        *lengthPointer = [someData length];
    }
    
    //Easy way
    return (uint8_t*)bytes;
}

- (void)runOnMainThread:(void (^)(void))code {
    if (code) {
        if ([[NSThread currentThread] isMainThread]) {
            code();
        } else {
            dispatch_async(dispatch_get_main_queue(), ^{
                code();
            });
        }
    }
}

@end
