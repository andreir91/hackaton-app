﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace Server {
    class StatusObject {
        public StreamWriter writer;
        public string clientName;

        public StatusObject(StreamWriter writer, string clientName) {
            this.writer = writer;
            this.clientName = clientName;
        }
    }
}
