package com.example.mateisuica.hackatonapp;

/**
 * Created by mateisuica on 06/06/15.
 */
public class ListItemDataHolder {
    public String title;
    public int imageResId;

    public ListItemDataHolder(String title, int imageResId) {
        this.title = title;
        this.imageResId = imageResId;
    }
}
