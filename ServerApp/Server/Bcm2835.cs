﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.InteropServices;

namespace Server
{
    class Bcm2835
    {

        #region Imported functions
        //Library Init
        [DllImport("libbcm2835.so", EntryPoint = "bcm2835_init")]
        static extern bool bcm2835_init();

        [DllImport("libbcm2835.so", EntryPoint = "bcm2835_close")]
        static extern bool bcm2835_close();

        //I2C external imports
        [DllImport("libbcm2835.so", EntryPoint = "bcm2835_i2c_begin")]
        static extern void bcm2835_i2c_begin();

        [DllImport("libbcm2835.so", EntryPoint = "bcm2835_i2c_setSlaveAddress")]
        static extern void bcm2835_i2c_setSlaveAddress(uint addr);

        [DllImport("libbcm2835.so", EntryPoint = "bcm2835_i2c_setClockDivider")]
        static extern void bcm2835_i2c_setClockDivider(UInt16 div);

        [DllImport("libbcm2835.so", EntryPoint = "bcm2835_i2c_set_baudrate")]
        static extern void bcm2835_i2c_set_baudrate(UInt32 baud);

        [DllImport("libbcm2835.so", EntryPoint = "bcm2835_i2c_write")]
        static extern void bcm2835_i2c_write(string data, UInt32 len);

        [DllImport("libbcm2835.so", EntryPoint = "bcm2835_i2c_write")]
        static extern void bcm2835_i2c_write(byte[] data, UInt32 len);

        [DllImport("libbcm2835.so", EntryPoint = "bcm2835_i2c_read")]
        static extern void bcm2835_i2c_read(byte[] data, UInt32 len);

        [DllImport("libbcm2835.so", EntryPoint = "bcm2835_i2c_end")]
        static extern void bcm2835_i2c_end();
        #endregion

        public void Init()
        {
            if (!bcm2835_init())
            {
                throw new Exception("Unable to initialize bcm2835.so library");
            }
        }

        public void Close()
        {
            bcm2835_close();
        }

        #region I2C functions low level

        private void I2C_Begin()
        {
            bcm2835_i2c_begin();
        }

        private void I2C_End()
        {
            bcm2835_i2c_end();
        }

        private void I2C_SetSlaveAddress(uint address)
        {
            bcm2835_i2c_setSlaveAddress(address);
        }

        private void I2C_SetClockDivider(UInt16 divider)
        {
            bcm2835_i2c_setClockDivider(divider);
        }

        private void I2C_SetBaudRate(UInt32 baudRate)
        {
            bcm2835_i2c_set_baudrate(baudRate);
        }

        private void I2C_Write(byte[] data)
        {
            bcm2835_i2c_write(data, (UInt32)data.Length);
        }

        private void I2C_Write(byte[] data, UInt32 lenght)
        {
            bcm2835_i2c_write(data, lenght);
        }

        private void I2C_Read(byte[] data, UInt32 lenght)
        {
            bcm2835_i2c_read(data, lenght);
        }
        #endregion

        #region I2C functions
        public void I2C_WriteBytes(uint address, UInt32 baud, byte[] data)
        {
            I2C_Begin();
            I2C_SetSlaveAddress(address);
            I2C_SetBaudRate(baud);
            I2C_Write(data, (UInt32)data.Length);
            I2C_End();
        }

        public void I2C_WriteBytes(uint address, UInt32 baud, byte[] data, UInt32 lenght)
        {
            I2C_Begin();
            I2C_SetSlaveAddress(address);
            I2C_SetBaudRate(baud);
            I2C_Write(data, lenght);
            I2C_End();
        }

        public byte[] I2C_ReadBytes(uint address, UInt32 baud, UInt32 lenght)
        {
            byte[] data = new byte[lenght];
            I2C_Begin();
            I2C_SetSlaveAddress(address);
            I2C_SetBaudRate(baud);
            I2C_Read(data, lenght);
            I2C_End();
            return data;
        }
        #endregion
    }
}
