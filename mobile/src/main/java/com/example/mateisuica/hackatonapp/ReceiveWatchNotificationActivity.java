package com.example.mateisuica.hackatonapp;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;

/**
 * Created by mateisuica on 06/06/15.
 */
public class ReceiveWatchNotificationActivity extends Activity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.notification_receiver);

        TextView textView = (TextView) findViewById(R.id.theId);
        Intent intent = getIntent();
        if(intent != null) {
            int id = intent.getIntExtra(SendWatchNotificationActivity.EXTRA_EVENT_ID, 0);
            textView.setText(Integer.toString(id));
        }
    }
}
