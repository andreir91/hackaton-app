//
//  NotificationController.h
//  myApp WatchKit Extension
//
//  Created by Boca Andrei on 07/06/2015.
//  Copyright (c) 2015 Boca Andrei. All rights reserved.
//

#import <WatchKit/WatchKit.h>
#import <Foundation/Foundation.h>

@interface NotificationController : WKUserNotificationInterfaceController

@end
