package services;

import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.google.android.gms.wearable.MessageEvent;
import com.google.android.gms.wearable.WearableListenerService;

import constants.Constants;

/**
 * Created by Andrei on 06/06/15.
 */
public class WearableConnectionService extends WearableListenerService{

    @Override
    public void onMessageReceived(MessageEvent messageEvent){
        // Send command to the server
        Intent intent = new Intent(Constants.WEAR_COMMAND);
        intent.putExtra("command", messageEvent.getPath());
        LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
    }
}