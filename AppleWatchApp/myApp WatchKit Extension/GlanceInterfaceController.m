//
//  GlanceInterfaceController.m
//  myApp
//
//  Created by Boca Andrei on 07/06/2015.
//  Copyright (c) 2015 Boca Andrei. All rights reserved.
//

#import "GlanceInterfaceController.h"

@interface GlanceInterfaceController ()
@property (weak, nonatomic) IBOutlet WKInterfaceMap *wkNa;

@property (weak, nonatomic) IBOutlet WKInterfaceLabel *statusText;
@property (nonatomic, strong)SCSocket *communicationSocket;
@end

@implementation GlanceInterfaceController

- (void)awakeWithContext:(id)context {
    [super awakeWithContext:context];
    
    self.communicationSocket = [SCSocket new];
    [self.communicationSocket startSocket];
    //[self.communicationSocket setContinuosString:@"status\r\n"];
    [self.communicationSocket addDelegate:self];
    // Configure interface objects here.
}

- (void)willActivate {
    [self.communicationSocket sendCommand:SCSocketCommandsLocation];
    // This method is called when watch view controller is about to be visible to user
    [super willActivate];
}

- (void)didDeactivate {
    [self.communicationSocket removeDelegate:self];
    [self.communicationSocket stopSocket];
    self.communicationSocket = nil;
    // This method is called when watch view controller is no longer visible
    [super didDeactivate];
}


- (void)socket:(SCSocket *)socket receivedData:(NSString *)dataReceived {
    if ([dataReceived containsString:@"location"]) {
        NSArray *coordonate = [[dataReceived componentsSeparatedByString:@"="][1] componentsSeparatedByString:@","];
        
        CLLocationCoordinate2D coordonates = CLLocationCoordinate2DMake([coordonate[0] doubleValue],[coordonate[1] doubleValue]);
        [self.wkNa setRegion:MKCoordinateRegionMakeWithDistance(coordonates, 200 , 200)];
        [self.wkNa addAnnotation:coordonates withPinColor:WKInterfaceMapPinColorRed];
    } else  if ([dataReceived containsString:@"status"]) {
        NSArray *data = [dataReceived componentsSeparatedByString:@"#"];
        //lockState
        NSMutableString *lockString = [NSMutableString new];
        [lockString appendString:([data[1] containsString:@"0"] ? @"Unlocked" : @"Locked")];
        //doorState
        [lockString appendString: ([data[3] containsString:@"0"] ? @" with doors opened" : @"")];
        
        //alarmState
        [lockString appendString:([data[2] containsString:@"1"] ? @"\nAlarm!" : @"")];
        
        [self.statusText setText:lockString];
    }
}
@end



