package com.example.mateisuica.hackatonapp;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.nfc.cardemulation.CardEmulation;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.widget.TextView;

/**
 * Created by mateisuica on 06/06/15.
 */
public class StatusActivity extends Activity {

    private TextView lock;
    private TextView alarm;
    private TextView doors;
    private TextView temp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.status_activity);

        lock = (TextView) findViewById(R.id.lockLabel);
        alarm = (TextView) findViewById(R.id.alarmLabel);
        doors = (TextView) findViewById(R.id.doorsLabel);
        temp = (TextView) findViewById(R.id.tempLabel);

        populateLabels();

        LocalBroadcastManager.getInstance(this).registerReceiver(mMessageReceiver,
                new IntentFilter("refresh-status"));

    }

    private void populateLabels() {
        CarDataSingleton carDataSingleton = CarDataSingleton.getInstance();
        lock.setText(carDataSingleton.isLock() ? "Locked" : "Unlocked");
        alarm.setText(carDataSingleton.isAlarm() ? "Alarm running" : "No alarm");
        doors.setText(carDataSingleton.isDoors() ? "Doors closed" : "Doors open");
        temp.setText(Integer.toString(carDataSingleton.getTemperature()));
    }

    private BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            populateLabels();
        }
    };
}
